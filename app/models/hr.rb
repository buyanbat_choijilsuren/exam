# frozen_string_literal: true

module Hr
  def self.table_name_prefix
    'hr_'
  end
end
