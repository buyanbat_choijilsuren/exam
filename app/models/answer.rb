class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question
  belongs_to :session
  belongs_to :choice, optional: true

  validates :user, uniqueness: { scope: %i[session question] }
  enum status: %i[pending correct incorrect]
end
