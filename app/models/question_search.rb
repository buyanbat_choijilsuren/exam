class QuestionSearch
  include ActiveModel::Model

  attr_reader :title, :category, :status, :user, :position, :multiple
  attr_writer :result

  def initialize(attrs)
    @result = Question
    super(attrs.select { |_, value| value.present? }.slice(:title, :category, :status, :result, :user, :position, :multiple))
  end

  def title=(val)
    @title = val
    q_ids = ActionText::RichText.where(record_type: 'Question', name: 'title')
                                .where('body like ?', "%#{val}%")
                                .pluck(:record_id)
    @result = @result.where(id: q_ids)
  end

  def category=(val)
    @category = val
    @result = @result.where(category: val)
  end

  def status=(val)
    @status = val
    @result = @result.where(status: val)
  end

  def user=(val)
    @user = val
    @result = @result.where(user_id: val)
  end

  def position=(val)
    @position = val
    @result = @result.joins(:question_positions).where(test_question_positions: { position_id: val })
  end

  def multiple=(val)
    @multiple = val
    @result = @result.where(multiple: val) if val.to_i == 1
  end

  def result
    @result.order(:id)
  end
end
