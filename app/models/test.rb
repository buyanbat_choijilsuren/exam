class Test < ApplicationRecord
  has_many :test_categories
  has_many :categories, through: :test_categories
  has_many :sessions, inverse_of: :test
  enum status: %i[active inactive]

  accepts_nested_attributes_for :test_categories, reject_if: ->(attributes){ attributes['question_count'].to_i.zero? }, allow_destroy: true

  def eligible?
    start_date <= Time.now && end_date >= Time.now
  end

  def questions_count
    test_categories.pluck(:question_count).reduce(:+)
  end
end
