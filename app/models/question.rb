class Question < ApplicationRecord
  belongs_to :category
  has_many :choices
  has_many :answers

  belongs_to :user
  belongs_to :confirmed_user, class_name: 'User', foreign_key: :confirmed_by, optional: true

  has_many :question_positions
  has_many :positions, through: :question_positions

  has_rich_text :title
  enum status: %i[active confirmed]

  accepts_nested_attributes_for :choices, allow_destroy: true, reject_if: :choice_rejectable?
  accepts_nested_attributes_for :positions

  private

  def choice_rejectable?(attrs)
    attrs['title'].blank?
  end
end
