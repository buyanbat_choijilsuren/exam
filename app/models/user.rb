# frozen_string_literal: true

class User < ApplicationRecord
  self.table_name = 'users'

  belongs_to :employee, primary_key: :code, foreign_key: :code, class_name: 'Hr::Employee'
  has_many :sessions

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :timeoutable

  def admin?
    %w[0332 0144 0306 0188 0001 0803 0710 0075 0002].include? code
  end

  def moderator?
    %w[0067 0144 0306 0188 0001 0644 0700 0156 0379 0068 0075 0584 0714 0783 0213 0376 0806 0803 0710].include? code
  end

  def self.work_groups
    %w[
      uyanga@capitronbank.mn
      tsendsuren.d@capitronbank.mn
      munkhtsetseg.b@capitronbank.mn
      khaliun.ch@capitronbank.mn
      nyamdorj.e@capitronbank.mn
      lkhagvatsend.j@capitronbank.mn
      uuganbuyan.b@capitronbank.mn
      utkhai.t@capitronbank.mn
      ujin@capitronbank.mn
      otgonbaatar@capitronbank.mn
      saranchimeg@capitronbank.mn
      battulga@capitronbank.mn
      ariuntuya_kh@capitronbank.mn
      nurgul.m@capitronbank.mn
      ariunjargal.g@capitronbank.mn
      temuujin@capitronbank.mn
      tulgaa@capitronbank.mn
      bayarmagnai.a@capitronbank.mn
      otgonchimeg.n@capitronbank.mn
    ]
  end
end
