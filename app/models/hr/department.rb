# frozen_string_literal: true

module Hr
  class Department < ApplicationRecord
    self.primary_key = :depid
  end
end
