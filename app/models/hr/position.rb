# frozen_string_literal: true

module Hr
  class Position < ApplicationRecord
    belongs_to :department, foreign_key: :depid, optional: true
    has_many :emp_position, inverse_of: :position, foreign_key: :position_id
    has_many :employees, through: :emp_position

    scope :active, -> { where('emp_count > 0').order(depid: :asc, emp_count: :desc) }

    def title
      if depid.present? && !depid.to_i.zero?
        [department.name, name, emp_count].join(' - ')
      else
        [name, emp_count].join(' - ')
      end
    end
  end
end
