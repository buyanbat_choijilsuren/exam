# frozen_string_literal: true

module Hr
  class Employee < ApplicationRecord
    self.primary_key = :code
    has_one :emp_position, inverse_of: :employee, foreign_key: :emp_id, primary_key: :empid
    has_one :position, through: :emp_position
  end
end
