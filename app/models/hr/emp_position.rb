# frozen_string_literal: true

module Hr
  class EmpPosition < ApplicationRecord
    belongs_to :position
    belongs_to :employee, foreign_key: :emp_id, primary_key: :empid
  end
end
