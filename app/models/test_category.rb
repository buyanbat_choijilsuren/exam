class TestCategory < ApplicationRecord
  belongs_to :test
  belongs_to :category
  has_many :questions, through: :category
end
