class Choice < ApplicationRecord
  belongs_to :question
  has_many :answers

  enum status: %i[active inactive]

  validates :title, presence: true
end
