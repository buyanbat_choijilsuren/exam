# frozen_string_literal: true

class SessionSearch
  include ActiveModel::Model

  attr_reader :user, :test, :position, :score
  attr_writer :result

  def initialize(attrs)
    @result = Session
    super(attrs.select { |_, value| value.present? }.slice(:user, :test, :position, :score))
  end

  def user=(val)
    @user = val
    @result = @result.joins(:user).where('upper(users.name) like ?', "%#{val.upcase}%")
  end

  def score=(val)
    @score = val
    @result = @result.where('percent >= ?', val)
  end

  def test=(val)
    @test = val
    @result = @result.where(test_id: val)
  end

  def result
    @result.ranked
  end
end
