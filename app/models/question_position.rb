class QuestionPosition < ApplicationRecord
  belongs_to :question
  belongs_to :position, class_name: 'Hr::Position', foreign_key: :position_id
end
