# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :test_categories
  has_many :test, through: :test
  has_many :questions
  enum status: %i[active inactive]

  def name
    title
  end

  def user_questions(user)
    assigned = questions.where(multiple: false)
                        .includes(:positions)
                        .where('hr_positions.id = ?', user.employee.position.id)
                        .pluck(:id)
                        .uniq

    (assigned + public_questions).uniq
  end

  def position_questions(position)
    assigned = questions.where(multiple: false)
                        .includes(:positions)
                        .where('hr_positions.id = ?', position.id)
                        .pluck(:id)
                        .uniq

    (assigned + public_questions).uniq
  end

  def public_questions
    questions.where(multiple: true).pluck(:id)
  end
end
