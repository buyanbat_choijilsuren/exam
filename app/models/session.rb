# frozen_string_literal: true

class Session < ApplicationRecord
  serialize :question_ids, Array
  belongs_to :user
  belongs_to :test
  has_many :answers, dependent: :destroy
  enum status: %i[created active finished]

  paginates_per 100
  scope :ranked, -> { order(percent: :desc, updated_at: :asc) }

  def questions
    Question.where(id: question_ids.flatten)
  end

  def eligible?
    active? && ends_at > (Time.now + 1.seconds)
  end

  def start!
    question_ids.clear
    test.test_categories.each do |tc|
      q_ids = tc.category.user_questions(user)
      question_ids << tc.questions
                      .where(id: q_ids)
                      .order('dbms_random.value')
                      .take(tc.question_count).pluck(:id)
    end

    if test.questions_count > question_ids.flatten.size
      # Хөдөлмөрийн дотоод журам
      fill_category = Category.find(10_026)
      q_ids = fill_category.user_questions(user)
      remaining_count = test.questions_count - question_ids.flatten.size

      question_ids << fill_category.questions
                      .where(id: (q_ids - question_ids.flatten))
                      .order('dbms_random.value')
                      .take(remaining_count).pluck(:id)
    end

    update(
      starts_at: Time.now,
      ends_at: Time.now + test.duration.minutes,
      duration: test.duration,
      status: :active
    )

    TestJob.set(wait: duration.minutes).perform_later(self)
  end

  def complete!
    total_score = 0
    answers.each do |answer|
      answer.update(correct: answer.choice&.correct)
      total_score += answer.question.category.score if answer.choice&.correct?
    end
    update(correct: answers.where(correct: true).count, status: :finished, percent: [100, total_score].min)
  end
end
