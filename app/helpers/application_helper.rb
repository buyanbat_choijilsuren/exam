# frozen_string_literal: true

module ApplicationHelper

  def flashy
    type = nil

    if (text = flash[:notice] || params[:notice])
      type = :notice
    elsif (text = flash[:error] || params[:error])
      type = :error
    elsif (text = flash[:warning] || params[:warning])
      type = :warning
    elsif (text = flash[:alert] || params[:alert])
      type = :alert
    end
    render(partial: 'shared/flash', locals: { text: text, type: type })
  end

  def pagination_range(paginated)
    total = paginated.total_count
    from = 1 + paginated.offset_value
    to = paginated.offset_value + paginated.current_per_page
    to = total if to > total
    "Нийт #{total} - аас #{from}-#{to} "
  end

  def current_menu
    @current_menu ||= controller.controller_name
  end
end
