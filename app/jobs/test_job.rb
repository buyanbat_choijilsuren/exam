class TestJob < ApplicationJob
  queue_as :default

  def perform(session)
    session.try(:complete!)
  end
end
