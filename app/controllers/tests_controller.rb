class TestsController < ApplicationController
  before_action :set_test, only: %i[show edit update destroy start answer]
  before_action :admin_required, only: %i[reset new edit update destroy]

  # GET /tests
  # GET /tests.json
  def index
    @tests = Test.all
  end

  # GET /tests/1
  # GET /tests/1.json
  def show; end

  # GET /tests/new
  def new
    @test = Test.new
    Category.active.each do |category|
      @test.test_categories.build(category: category)
    end
  end

  # GET /tests/1/edit
  def edit
    Category.active.each do |category|
      @test.test_categories.build(category: category) unless @test.category_ids.include?(category.id)
    end
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = Test.new(test_params)

    respond_to do |format|
      if @test.save
        format.html { redirect_to tests_path, notice: 'Test was successfully created.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    respond_to do |format|
      if @test.update(test_params)
        format.html { redirect_to tests_path, notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to tests_url, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def start
    @session = @test.sessions.find_or_create_by(user: current_user)
    if @session.created? && @test.eligible?
      @session.start!
    elsif !@session.eligible?
      redirect_to @session
    end
  end

  def answer
    @session = @test.sessions.find_by(user: current_user)
    if @session&.eligible?
      @answer = Answer.find_or_initialize_by(answer_params)
      @answer.update(choice_id: params[:answer][:choice_id])
      render json: :success
    else
      render json: :error
    end
  end

  def reset
    if request.post?
      @user = User.find_by(reset_params)
      if @user
        @password = (0...6).map { rand(65..91).chr }.join
        @user.reset_password(@password, @password)
      else
        render notice: 'Хэрэглэгч олдсонгүй!'
      end
    else
      @user = User.new
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_test
    @test = Test.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def test_params
    params.fetch(:test, {}).permit(:name, :start_date, :end_date, :duration, :status, test_categories_attributes: %i[category_id question_count _destroy id])
  end

  def answer_params
    params.fetch(:answer, {}).permit(:question_id).merge(user_id: current_user.id, session_id: @session.id)
  end

  def reset_params
    params.fetch(:user, {}).permit(:email)
  end
end
