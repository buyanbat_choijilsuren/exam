# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def admin_required
    redirect_to root_path, notice: 'Хэрэглэгчийн эрх хүрэхгүй байна!' unless current_user.admin?
  end

  def moderator_required
    redirect_to root_path, notice: 'Хэрэглэгчийн эрх хүрэхгүй байна!' unless (current_user.admin? || current_user.moderator?)
  end
end
