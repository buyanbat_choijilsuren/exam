class QuestionsController < ApplicationController
  before_action :set_question, only: %i[show edit update destroy]
  before_action :moderator_required

  # GET /questions
  # GET /questions.json
  def index
    @filter = QuestionSearch.new(filter_params)
    @questions = @filter.result.page(params[:page] || 0)
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
    if (last = Question.where(user: current_user).last)
      @question.position_ids = last.position_ids
    end
    4.times { @question.choices.build }
  end

  # GET /questions/1/edit
  def edit
    (4 - @question.choices.count).times { @question.choices.build }
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def dashboard
    expires_in 5.minutes
    @positions = Hr::Position.active
    @categories = Category.all
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_question
    @question = Question.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def question_params
    params.fetch(:question, {}).permit(:title, :category_id, :status, :source, :multiple, choices_attributes: %i[id title correct _destroy], position_ids: [])
  end

  def filter_params
    if current_user.admin?
      params.fetch(:search, {}).permit(:title, :status, :category, :user, :position, :multiple)
    else
      params.fetch(:search, {}).permit(:title, :status, :category, :position, :user, :multiple).merge(user: current_user)
    end
  end
end
