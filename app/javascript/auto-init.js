import mdcAutoInit from '@material/auto-init';
import {MDCTextField} from '@material/textfield';
import {MDCSwitch} from '@material/switch';
import {MDCSelect} from '@material/select';
import {MDCRipple} from '@material/ripple';
import {MDCChipSet, MDCChip} from '@material/chips';
import flatpick from 'flatpickr';
import flatpickr from 'flatpickr';

mdcAutoInit.register('text-field', MDCTextField);
mdcAutoInit.register('switch-field', MDCSwitch);
mdcAutoInit.register('select-field', MDCSelect);
mdcAutoInit.register('ripple', MDCRipple);
mdcAutoInit.register('chip-set', MDCChipSet);
mdcAutoInit.register('chip', MDCChip);

mdcAutoInit();

flatpickr(document.querySelectorAll('input.flatpickr'), {
  enableTime: true
});