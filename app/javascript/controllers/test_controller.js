import { Controller } from "stimulus"
import { MDCList } from '@material/list'
import Rails from '@rails/ujs';
import Swal from 'sweetalert2';

export default class extends Controller {
  static targets = ['status', 'question', 'marker', 'timer', 'progress']
  initialize(){
    this.ends_at =  parseInt(this.timerTarget.textContent)
    this.interval = setInterval(() => {
      this.ends_at --
      var hour = `0${parseInt(this.ends_at / 60)}`.substr(-2)
      var minute = `0${parseInt(this.ends_at % 60)}`.substr(-2)
      this.timerTarget.textContent = `${hour} : ${minute}`
      if (this.ends_at < 0){
        clearInterval(this.interval)
        Swal.fire({
          type: 'success',
          title: 'Шалгалтын цаг дууслаа!',
          showConfirmButton: false,
          timer: 2500
        }).then(() => {
          location.reload();
        })
      }
    }, 1000)
    this.questionTargets.forEach((el, i) => {
      var form = el.querySelector('form');
      var list = new MDCList(el.querySelector('ul.mdc-list'))
      list.singleSelection = true;
      form.addEventListener('change', (event) => {
        this.update_progress();
        Rails.fire(form, 'submit');
      });
    });
    this.show();
    this.update_progress();
  }

  get index(){
    return parseInt(this.data.get('index') || 0)
  }

  set index(val){
    this.data.set('index', val)
    this.show()
  }
  show(){
    this.statusTarget.textContent = [(this.index + 1), this.questionTargets.length].join(' / ')
    this.questionTargets.forEach((el, i) => {
      el.classList.toggle('hide', i !== this.index)
    })
    this.markerTargets.forEach((el, i) => {
      el.classList.toggle('mdc-list-item--selected', i === this.index)
      if (this.questionTargets[i].querySelector('input[type=radio]:checked')){
        el.querySelector('.material-icons').textContent = 'check_box'
      }else{
        el.querySelector('.material-icons').textContent = 'check_box_outline_blank'
      }
    })
  }
  prev(){
    if(this.index > 0){
      this.index -- 
    }
  }
  next(){
    if(this.index < this.questionTargets.length - 1){
      this.index ++
    }
  }

  select(event){
    this.index = event.currentTarget.dataset.index;
  }

  update_progress(){
    const progress = this.element.querySelectorAll('input[type=radio]:checked').length;
    this.progressTarget.textContent = progress;
  }
}