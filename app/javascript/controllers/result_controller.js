import { Controller } from "stimulus"
import Rails from '@rails/ujs';

export default class extends Controller {
  static targets = ['status', 'question', 'marker']
  initialize(){   
    this.show();
  }

  get index(){
    return parseInt(this.data.get('index') || 0)
  }

  set index(val){
    this.data.set('index', val)
    this.show()
  }
  show(){
    this.statusTarget.textContent = [(this.index + 1), this.questionTargets.length].join(' / ')
    this.questionTargets.forEach((el, i) => {
      el.classList.toggle('hide', i !== this.index)
    })
    this.markerTargets.forEach((el, i) => {
      el.classList.toggle('mdc-list-item--selected', i === this.index)
    })
  }
  prev(){
    if(this.index > 0){
      this.index -- 
    }
  }
  next(){
    if(this.index < this.questionTargets.length - 1){
      this.index ++
    }
  }

  select(event){
    this.index = event.currentTarget.dataset.index;
  }
}