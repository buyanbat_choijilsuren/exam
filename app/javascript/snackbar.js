import { MDCSnackbar } from '@material/snackbar';

class Snackbar {
  constructor(){
    this.element = document.querySelector('#crm-flash-snackbar');
    this.snackbar = new MDCSnackbar(this.element);
    if(this.snackbar.labelText){
      this.snackbar.open()
    }
  }
  
  info(text, timeout){
    this.snackbar.labelText = text;
    this.snackbar.timeoutMs = timeout || 4000; 
    this.snackbar.open();
  }
}

var los_snackbar = new Snackbar();

export { los_snackbar };
