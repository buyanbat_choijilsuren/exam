import Rails from '@rails/ujs';
import Swal from 'sweetalert2';

Rails.confirm = (message, element) => {
  Swal.fire({
    title: message,
    text: '',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: `<i class='material-icons mdc-button__icon'>check</i> Тийм, итгэлтэй байна!`,
    cancelButtonText: `<i class='material-icons mdc-button__icon'>clear</i> Үгүй!`,
    buttonsStyling: false,
    focusConfirm: false,
    customClass: {
      confirmButton: 'mdc-button mdc-button--raised mr3',
      cancelButton: 'mdc-button ml3',
    }
  }).then((result) => {
    if (result.value){
      element.removeAttribute('data-confirm');
      element.click();
    }
  })
}



