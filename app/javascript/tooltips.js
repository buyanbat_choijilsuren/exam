import tippy from 'tippy.js';

tippy.setDefaultProps({
  animation: 'scale',
})

tippy('[data-tippy-content]')