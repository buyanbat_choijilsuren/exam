# frozen_string_literal: true

class ActionView::Helpers::FormBuilder
  def error_message_for(field_name)
    if self.object.errors[field_name].present?
      model_name              = self.object.class.name.downcase
      id_of_element           = "error_#{model_name}_#{field_name}"
      target_elem_id          = "#{model_name}_#{field_name}"
      class_name              = 'mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg mdc-text-field-helper-text--invalid-msg'
      #error_declaration_class = 'mdc-text-field--invalid'

      @template.content_tag(:p, self.object.errors[field_name].join(', '), id: id_of_element, class: class_name, for: target_elem_id)
    end
  end
end
