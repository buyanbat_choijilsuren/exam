# frozen_string_literal: true
#
# Uncomment this and change the path if necessary to include your own
# components.
# See https://github.com/plataformatec/simple_form#custom-components to know
# more about custom components.
#
# Use this setup block to configure all options available in SimpleForm.
Dir[Rails.root.join('lib/components/**/*.rb')].each { |f| require f }

SimpleForm.setup do |config|
  # Wrappers are used by the form builder to generate a
  # complete input. You can remove any component from the
  # wrapper, change the order or even add your own to the
  # stack. The options given below are used to wrap the
  # whole input.
  config.label_text = ->(label, required, _) { [label, required].join(' ') }

  config.include_default_input_wrapper_class = false


  config.wrappers :default,
                  tag: 'div',
                  class: 'mdc-text-field mdc-text-field--dense mdc-text-field--with-leading-icon',
                  error_class: 'mdc-text-field--invalid',
                  wrapper_html: { data: { 'mdc-auto-init' => 'text-field' } } do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :minlength
    b.optional :pattern
    b.optional :min_max
    b.optional :autocomplete
    b.optional :readonly
    b.use :input, class: 'mdc-text-field__input', error_class: 'is-invalid', valid_class: 'is-valid'
    b.use :label, class: 'mdc-floating-label'
    b.optional :icon
    b.wrapper tag: 'div', class: 'mdc-line-ripple' do; end
  end

  config.wrappers :mdc_select,
                  tag: 'div',
                  class: 'mdc-select mdc-select--with-leading-icon',
                  error_class: 'mdc-select--invalid',
                  wrapper_html: { data: { 'mdc-auto-init' => 'select-field' } } do |b|
    b.use :html5
    b.optional :readonly
    b.wrapper tag: 'i', class: 'mdc-select__dropdown-icon' do; end
    b.use :icon, class: 'mdc-select__icon'

    b.use :input, class: 'mdc-select__native-control'
    b.use :label, class: 'mdc-floating-label'
    b.wrapper tag: 'div', class: 'mdc-line-ripple' do; end
  end

  config.wrappers :mdc_enhanced,
                  tag: 'div',
                  class: 'mdc-select mdc-select--with-leading-icon',
                  error_class: 'mdc-select--invalid',
                  wrapper_html: { data: { 'mdc-auto-init' => 'select-field' } } do |b|
    b.use :html5
    b.optional :readonly
    b.use :icon, class: 'mdc-select__icon'
    b.wrapper tag: 'i', class: 'mdc-select__dropdown-icon' do; end
    b.wrapper tag: 'div', class: 'mdc-select__selected-text' do; end
    b.use :input
    b.use :label, class: 'mdc-floating-label'
    b.wrapper tag: 'div', class: 'mdc-line-ripple' do; end
  end

  config.wrappers :mdc_textarea, tag: 'div', class: 'mdc-text-field mdc-text-field--textarea mdc-text-field--with-trailing-icon', error_class: 'mdc-text-field--invalid', wrapper_html: { data: {'mdc-auto-init' => 'text-field' } } do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :minlength
    b.optional :pattern
    b.optional :min_max
    b.optional :autocomplete
    b.optional :readonly

    b.use :input, class: 'mdc-text-field__input', error_class: 'is-invalid', valid_class: 'is-valid'
    b.optional :icon

    b.wrapper tag: 'div', class: 'mdc-notched-outline' do |bb|
      bb.wrapper tag: 'div', class: 'mdc-notched-outline__leading' do; end
      bb.wrapper tag: 'div', class: 'mdc-notched-outline__notch' do |bl|
        bl.use :label, class: 'mdc-floating-label'
      end
      bb.wrapper tag: 'div', class: 'mdc-notched-outline__trailing' do; end
    end
  end

   # vertical input for boolean
  config.wrappers :mdc_switch, tag: 'div', class: 'mdc-switch', wrapper_html: { data: { 'mdc-auto-init' => 'switch-field' } } do |b|
    b.use :html5
    b.optional :readonly
    b.wrapper tag: 'div', class: 'mdc-switch__track' do; end
    b.wrapper tag: 'div', class: 'mdc-switch__thumb-underlay' do |bc|
      bc.wrapper tag: 'div', class: 'mdc-switch__thumb' do |bt|
        bt.use :input, class: 'mdc-switch__native-control', role: 'switch'
      end
    end
  end
  # The default wrapper to be used by the FormBuilder.
  config.default_wrapper = :default

  # Define the way to render check boxes / radio buttons with labels.
  # Defaults to :nested for bootstrap config.
  #   inline: input + label
  #   nested: label > input
  config.boolean_style = :nested

  # Default class for buttons
  config.button_class = 'mdc-button mdc-button--raised'

  # Method used to tidy up errors. Specify any Rails Array method.
  # :first lists the first message for each field.
  # Use :to_sentence to list all errors for each field.
  # config.error_method = :first

  # Default tag used for error notification helper.
  config.error_notification_tag = :div

  # CSS class to add for error notification helper.
  config.error_notification_class = ' crm-form--errors'

  # Series of attempts to detect a default label method for collection.
  # config.collection_label_methods = [ :to_label, :name, :title, :to_s ]

  # Series of attempts to detect a default value method for collection.
  # config.collection_value_methods = [ :id, :to_s ]

  # You can wrap a collection of radio/check boxes in a pre-defined tag, defaulting to none.
  # config.collection_wrapper_tag = nil

  # You can define the class to use on all collection wrappers. Defaulting to none.
  # config.collection_wrapper_class = nil

  # You can wrap each item in a collection of radio/check boxes with a tag,
  # defaulting to :span.
  # config.item_wrapper_tag = :span

  # You can define a class to use in all item wrappers. Defaulting to none.
  # config.item_wrapper_class = nil

  # How the label text should be generated altogether with the required text.
  # config.label_text = lambda { |label, required, explicit_label| "#{required} #{label}" }

  # You can define the class to use on all labels. Default is nil.
  # config.label_class = nil

  # You can define the default class to be used on forms. Can be overriden
  # with `html: { :class }`. Defaulting to none.
  # config.default_form_class = nil

  # You can define which elements should obtain additional classes
  # config.generate_additional_classes_for = [:wrapper, :label, :input]

  # Whether attributes are required by default (or not). Default is true.
  # config.required_by_default = true

  # Tell browsers whether to use the native HTML5 validations (novalidate form option).
  # These validations are enabled in SimpleForm's internal config but disabled by default
  # in this configuration, which is recommended due to some quirks from different browsers.
  # To stop SimpleForm from generating the novalidate option, enabling the HTML5 validations,
  # change this configuration to true.
  config.browser_validations = false

  # Collection of methods to detect if a file type was given.
  # config.file_methods = [ :mounted_as, :file?, :public_filename, :attached? ]

  # Custom mappings for input types. This should be a hash containing a regexp
  # to match as key, and the input type that will be used when the field name
  # matches the regexp as value.
  # config.input_mappings = { /count/ => :integer }

  # Custom wrappers for input types. This should be a hash containing an input
  # type as key and the wrapper that will be used for all inputs with specified type.
  config.wrapper_mappings = {
    select: :mdc_select,
    text: :mdc_textarea,
    boolean: :mdc_switch,
    enhanced_select: :mdc_enhanced
  }

  # Namespaces where SimpleForm should look for custom input classes that
  # override default inputs.
  # config.custom_inputs_namespaces << "CustomInputs"

  # Default priority for time_zone inputs.
  # config.time_zone_priority = nil

  # Default priority for country inputs.
  # config.country_priority = nil

  # When false, do not use translations for labels.
  # config.translate_labels = true

  # Automatically discover new inputs in Rails' autoload path.
  # config.inputs_discovery = true

  # Cache SimpleForm inputs discovery
  # config.cache_discovery = !Rails.env.development?

  # Default class for inputs
  # config.input_class = nil

  # Define the default class of the input wrapper of the boolean input.
  config.boolean_label_class = 'checkbox'

  # Defines if the default input wrapper class should be included in radio
  # collection wrappers.
  # config.include_default_input_wrapper_class = true

  # Defines which i18n scope will be used in Simple Form.
  # config.i18n_scope = 'simple_form'

  # Defines validation classes to the input_field. By default it's nil.
  # config.input_field_valid_class = 'is-valid'
  # config.input_field_error_class = 'is-invalid'
end
