ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

ENV['NLS_LANG'] = 'AMERICAN_AMERICA.AL32UTF8'
DEFAULT_OCI8_ENCODING = 'utf-8'.freeze

require 'bundler/setup' # Set up gems listed in the Gemfile.
require 'bootsnap/setup' # Speed up boot time by caching expensive operations.
