Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :tests do
    get :start, on: :member
    put :answer, on: :member

    get :reset, on: :collection
    post :reset, on: :collection
  end

  resources :questions do
    get :dashboard, on: :collection
  end
  resources :sessions
  resources :categories
  resources :answers

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  root 'tests#index'
end
