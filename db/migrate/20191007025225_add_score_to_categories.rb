class AddScoreToCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :test_categories, :score, :integer, default: 2
  end
end
