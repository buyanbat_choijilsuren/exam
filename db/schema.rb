# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_07_025225) do

  create_table "accesses", force: :cascade do |t|
    t.string "accessible_type"
    t.integer "accessible_id", precision: 38
    t.string "item_type"
    t.integer "item_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.integer "record_id", precision: 38, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_unique", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", limit: 19, precision: 19, null: false
    t.integer "blob_id", limit: 19, precision: 19, null: false
    t.datetime "created_at", precision: 6, null: false
    t.index ["blob_id"], name: "i_act_sto_att_blo_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.integer "byte_size", limit: 19, precision: 19, null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: 6, null: false
    t.index ["key"], name: "i_active_storage_blobs_key", unique: true
  end

  create_table "app_orders", force: :cascade do |t|
    t.datetime "date", precision: 6
    t.integer "userid", precision: 38
    t.integer "depid", precision: 38
    t.string "name"
    t.string "purpose"
    t.string "scope"
    t.string "problem"
    t.string "functional_requirement"
    t.string "non_functional_requirement"
    t.integer "status", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "apps", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "icon"
    t.string "session_url"
    t.integer "status", precision: 38, default: 0
    t.string "key_path"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "details"
    t.string "color"
    t.integer "view_count", precision: 38, default: 0
  end

  create_table "article_categories", force: :cascade do |t|
    t.string "slug"
    t.string "name"
    t.integer "status", precision: 38, default: 0
    t.integer "ordering", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.text "brief"
    t.text "body"
    t.integer "user_id", precision: 38
    t.integer "status", precision: 38, default: 0
    t.integer "ordering", precision: 38, default: 0
    t.boolean "is_special", default: false
    t.integer "view_count", precision: 38, default: 0
    t.integer "like_count", precision: 38, default: 0
    t.integer "dislike_count", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "category_id", precision: 38
  end

  create_table "assignments", force: :cascade do |t|
    t.integer "user_id", precision: 38
    t.integer "role_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "commentable_type"
    t.integer "commentable_id", precision: 38
    t.integer "user_id", precision: 38
    t.text "body"
    t.integer "status", precision: 38, default: 0
    t.integer "parent_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "committees", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "estate_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "event_users", force: :cascade do |t|
    t.integer "event_id", limit: 19, precision: 19
    t.integer "user_id", limit: 19, precision: 19
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["event_id"], name: "i_events_users_events_id"
    t.index ["user_id"], name: "index_events_users_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "location"
    t.text "details"
    t.datetime "starts_at", precision: 6
    t.datetime "ends_at", precision: 6
    t.integer "category", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.boolean "public", default: false
    t.integer "user_id", limit: 19, precision: 19
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "details"
    t.integer "member_count", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "hr_emp_positions", force: :cascade do |t|
    t.integer "position_id", precision: 38
    t.integer "emp_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "hr_emppictures", temporary: true, id: false, force: :cascade do |t|
    t.decimal "empid"
    t.binary "picdata"
  end

  create_table "hr_pictures", temporary: true, id: false, force: :cascade do |t|
    t.decimal "empid"
    t.binary "picturedata"
  end

  create_table "hr_positions", force: :cascade do |t|
    t.string "name"
    t.integer "emp_count", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "likes", force: :cascade do |t|
    t.string "likeable_type"
    t.integer "likeable_id", precision: 38
    t.integer "user_id", precision: 38
    t.integer "status", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "loan_application_actions", force: :cascade do |t|
    t.integer "user_id", precision: 38
    t.integer "application_id", precision: 38
    t.date "action_date"
    t.text "reason"
    t.integer "status", precision: 38, default: 0
    t.integer "action", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "loan_application_products", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.integer "max_size", precision: 38
    t.decimal "interest", precision: 23, scale: 2
    t.integer "max_length", precision: 38
    t.integer "status", precision: 38, default: 0
    t.string "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "loan_applications", force: :cascade do |t|
    t.integer "user_id", precision: 38
    t.string "app_date"
    t.integer "product_id", precision: 38
    t.string "purpose"
    t.integer "amount", precision: 38
    t.integer "length", precision: 38
    t.text "details"
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "approve_user_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "membershipable_id", limit: 19, precision: 19
    t.integer "user_id", limit: 19, precision: 19
    t.integer "status", precision: 38, default: 0
    t.integer "role", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "membershipable_type"
    t.integer "creator_id", precision: 38
    t.integer "updated_user_id", precision: 38
    t.index ["membershipable_id"], name: "i_mem_mem_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "user_id", limit: 19, precision: 19
    t.integer "actor_id", limit: 19, precision: 19
    t.string "action"
    t.datetime "read_at", precision: 6
    t.string "notifiable_type"
    t.integer "notifiable_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["actor_id"], name: "i_notifications_actor_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "odoo_project_tasks", force: :cascade do |t|
    t.integer "task_id", precision: 38
    t.integer "project_id", precision: 38
    t.string "project"
    t.string "name"
    t.string "date_deadline"
    t.integer "user_id", precision: 38
    t.boolean "is_new"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "stage_id", precision: 38
    t.string "stage"
  end

  create_table "partners", force: :cascade do |t|
    t.integer "partnerable_id", precision: 38
    t.string "partnerable_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "poll_answers", force: :cascade do |t|
    t.integer "poll_id", limit: 19, precision: 19
    t.integer "user_id", limit: 19, precision: 19
    t.integer "poll_choice_id", limit: 19, precision: 19
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["poll_choice_id"], name: "i_poll_answers_poll_choice_id"
    t.index ["poll_id"], name: "index_poll_answers_on_poll_id"
    t.index ["user_id"], name: "index_poll_answers_on_user_id"
  end

  create_table "poll_choices", force: :cascade do |t|
    t.integer "poll_id", limit: 19, precision: 19
    t.string "title"
    t.integer "count", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["poll_id"], name: "index_poll_choices_on_poll_id"
  end

  create_table "polls", force: :cascade do |t|
    t.string "title"
    t.integer "status", precision: 38, default: 0
    t.datetime "starts_at", precision: 6
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "survey_id", precision: 38
    t.integer "kind", precision: 38, default: 0
  end

  create_table "posters", force: :cascade do |t|
    t.string "name"
    t.datetime "starts_at", precision: 6
    t.datetime "ends_at", precision: 6
    t.integer "status", precision: 38, default: 0
    t.integer "user_id", limit: 19, precision: 19
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_posters_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.integer "group_id", limit: 19, precision: 19
    t.integer "user_id", limit: 19, precision: 19
    t.text "post"
    t.integer "post_type", precision: 38, default: 0
    t.integer "view_count", precision: 38, default: 0
    t.integer "like_count", precision: 38, default: 0
    t.integer "comment_count", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["group_id"], name: "index_posts_on_group_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "progresses", force: :cascade do |t|
    t.integer "progressable_id", precision: 38
    t.string "progressable_type"
    t.integer "progress", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "user_id"
    t.text "description"
    t.integer "subprogressable_id", precision: 38
    t.text "subprogressable_type"
    t.datetime "progress_date", precision: 6
  end

  create_table "projects", force: :cascade do |t|
    t.string "projectable_type"
    t.integer "projectable_id", precision: 38
    t.integer "manager_id", precision: 38
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_business_plan", default: false
  end

  create_table "real_estates", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.integer "square", precision: 38
    t.integer "estate_type_id", limit: 19, precision: 19
    t.integer "status", precision: 38, default: 0
    t.integer "user_id", limit: 19, precision: 19
    t.integer "price", precision: 38
    t.integer "view_count", precision: 38, default: 0
    t.integer "ordering", precision: 38, default: 0
    t.integer "comment_count", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["estate_type_id"], name: "i_real_estates_estate_type_id"
    t.index ["user_id"], name: "index_real_estates_on_user_id"
  end

  create_table "regulation_attachments", force: :cascade do |t|
    t.integer "regulation_id", limit: 19, precision: 19
    t.string "name"
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["regulation_id"], name: "i_reg_att_reg_id"
  end

  create_table "regulation_categories", force: :cascade do |t|
    t.string "name"
    t.string "details"
    t.string "icon"
    t.integer "ordering", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "category_type", precision: 38, default: 0
    t.integer "parent_id", precision: 38
  end

  create_table "regulation_versions", force: :cascade do |t|
    t.integer "regulation_id", limit: 19, precision: 19
    t.string "description", limit: 2500
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", precision: 38
    t.index ["regulation_id"], name: "i_reg_ver_reg_id"
  end

  create_table "regulations", force: :cascade do |t|
    t.string "name"
    t.string "details"
    t.string "icon"
    t.integer "category_id", precision: 38
    t.integer "ordering", precision: 38, default: 0
    t.integer "status", precision: 38, default: 0
    t.integer "view_count", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "regulation_type", precision: 38, default: 0
    t.integer "access_type", precision: 38, default: 0
    t.date "approved_date"
    t.date "implemented_date"
    t.string "description", limit: 4000
    t.string "checksum"
    t.integer "user_id", precision: 38
  end

  create_table "role_accesses", force: :cascade do |t|
    t.string "accessible_type"
    t.integer "accessible_id", precision: 38
    t.integer "role_id", precision: 38
    t.integer "status", precision: 38, default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "structures", force: :cascade do |t|
    t.integer "depid", precision: 38
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "details"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer "tag_id", precision: 38
    t.string "taggable_type"
    t.integer "taggable_id", limit: 19, precision: 19
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["taggable_type", "taggable_id"], name: "i_tag_tag_typ_tag_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "task_boards", force: :cascade do |t|
    t.integer "title", precision: 38
    t.integer "description", precision: 38
    t.integer "user_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "board_type", precision: 38
  end

  create_table "task_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", precision: 38, null: false
    t.integer "descendant_id", precision: 38, null: false
    t.integer "generations", precision: 38, null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "task_anc_desc_idx", unique: true
    t.index ["descendant_id"], name: "task_desc_idx"
  end

  create_table "task_kanbans", force: :cascade do |t|
    t.integer "kanbanable_id", precision: 38
    t.string "kanbanable_type"
    t.integer "position", precision: 38
    t.integer "user_id", precision: 38
    t.integer "task_list_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "task_lists", force: :cascade do |t|
    t.integer "task_board_id", precision: 38
    t.integer "position", precision: 38
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "tasklistable_id", precision: 38
    t.string "tasklistable_type"
  end

  create_table "tasks", force: :cascade do |t|
    t.integer "project_id", precision: 38
    t.text "description"
    t.datetime "start_date", precision: 6
    t.datetime "end_date", precision: 6
    t.integer "assignee_id", precision: 38
    t.integer "user_id", precision: 38
    t.integer "percent", precision: 38
    t.integer "weight", precision: 38
    t.integer "status", precision: 38, default: 0
    t.integer "parent_id", precision: 38
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "priority", precision: 38
    t.integer "updated_user_id", precision: 38
    t.integer "due_days", precision: 38
    t.integer "task_type", precision: 38
  end

  create_table "test_answers", force: :cascade do |t|
    t.integer "question_id", limit: 19, precision: 19
    t.integer "choice_id", limit: 19, precision: 19
    t.integer "user_id", limit: 19, precision: 19
    t.integer "session_id", precision: 38
    t.boolean "correct"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["choice_id"], name: "i_test_answers_choice_id"
    t.index ["question_id"], name: "i_test_answers_question_id"
    t.index ["user_id"], name: "index_test_answers_on_user_id"
  end

  create_table "test_categories", force: :cascade do |t|
    t.string "title"
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "score", precision: 38, default: 2
  end

  create_table "test_choices", force: :cascade do |t|
    t.integer "question_id", limit: 19, precision: 19
    t.string "title"
    t.integer "status", precision: 38, default: 0
    t.boolean "correct", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "i_test_choices_question_id"
  end

  create_table "test_question_positions", force: :cascade do |t|
    t.integer "question_id", limit: 19, precision: 19
    t.integer "position_id", limit: 19, precision: 19
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["position_id"], name: "i_tes_que_pos_pos_id"
    t.index ["question_id"], name: "i_tes_que_pos_que_id"
  end

  create_table "test_questions", force: :cascade do |t|
    t.string "title", limit: 2550
    t.integer "status", precision: 38, default: 0
    t.boolean "multiple", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "category_id", precision: 38
    t.integer "user_id", precision: 38
    t.integer "confirmed_by", precision: 38
    t.string "source"
  end

  create_table "test_sessions", force: :cascade do |t|
    t.integer "user_id", limit: 19, precision: 19
    t.text "question_ids"
    t.datetime "starts_at", precision: 6
    t.datetime "ends_at", precision: 6
    t.integer "duration", precision: 38, default: 60
    t.integer "status", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "test_id", precision: 38
    t.integer "correct", precision: 38, default: 0
    t.integer "percent", precision: 38
    t.index ["user_id"], name: "index_test_sessions_on_user_id"
  end

  create_table "test_test_categories", force: :cascade do |t|
    t.integer "test_id", limit: 19, precision: 19
    t.integer "category_id", limit: 19, precision: 19
    t.integer "question_count", precision: 38, default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "i_tes_tes_cat_cat_id"
    t.index ["test_id"], name: "i_test_test_categories_test_id"
  end

  create_table "test_tests", force: :cascade do |t|
    t.string "name"
    t.integer "status", precision: 38, default: 0
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "duration", precision: 38, default: 60
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: 6
    t.datetime "remember_created_at", precision: 6
    t.string "code"
    t.string "userno"
    t.string "name"
    t.string "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "sso_key"
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: 6
    t.datetime "confirmation_sent_at", precision: 6
    t.index ["confirmation_token"], name: "i_users_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "i_users_reset_password_token", unique: true
  end

  add_foreign_key "poll_answers", "poll_choices"
  add_foreign_key "poll_answers", "polls"
  add_foreign_key "poll_answers", "users"
  add_foreign_key "poll_choices", "polls"
  add_foreign_key "posters", "users"
  add_foreign_key "real_estates", "estate_types"
  add_foreign_key "real_estates", "users"
end
