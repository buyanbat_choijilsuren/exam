# frozen_string_literal: true

module IconComponent
  def icon(wrapper_options = nil)
    @icon ||= begin
      content_tag(:i, options[:icon] || 'check', class: %w[material-icons mdc-text-field__icon].push(wrapper_options[:class])).html_safe
    end
  end
end

module SearchIconComponent
  def search_icon(_ = nil)
    @search_icon ||= begin
      content_tag(:i, options[:search_icon], class: 'material-icons mdc-text-field__icon crm-search-field__icon').html_safe if options[:search_icon].present?
    end
  end
end

SimpleForm.include_component(IconComponent)
SimpleForm.include_component(SearchIconComponent)
