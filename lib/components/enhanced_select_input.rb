class EnhancedSelectInput < SimpleForm::Inputs::CollectionInput
  def input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    @builder.hidden_field(attribute_name, merged_input_options) << (
      template.content_tag(:div, class: 'mdc-select__menu mdc-menu mdc-menu-surface') do
        template.concat list
      end
    )
  end

  def list
    label_method, value_method = detect_collection_methods
    template.content_tag(:ul, class: 'mdc-list') do
      collection.each do |option|
        value = option.try(value_method)
        text = option.try(label_method)
        template.concat option_tag(value, text)
      end
    end
  end

  def option_tag(value, text, selected = false)
    template.content_tag(:li, text, class: (selected ? 'mdc-list-item mdc-list-item--selected' : 'mdc-list-item'), data: {value: value}, role: 'option')
  end
end
